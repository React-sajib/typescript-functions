console.log('success')
// let a;

// a= 10

// console.log(a)


// "string"
// const obj = { width: 10, height: 15 };
// const area = obj.width * obj.height;

// console.log(area)

// let helloWorld = "Hello World"


// console.log(helloWorld,)

// interface User {
//  name: string;
//  id: number;
// }

// const user:User = {
//  name: "sajib",
//  id: 0,
// };

// console.log(user)

// interface User {
//  name: string;
//  id: number;
// }

// class UserAccount implements User {
//  name: string;
//  id: number;

//  constructor(name: string, id: number) {
//    this.name = name;
//    this.id = id;
//  }

 

// }

// const user = new UserAccount("Murphy", 1);

// console.log(user)


//  class functjion {
 

//  constructor(){
 
//  }
//     name(a:number,b:number){
  
//  let result = ab

// return result
//  }

// }

// let functjions = new functjion()

// let fdata = functjions.name(10,6)


// console.log(fdata,'here is f data')



// let order = "NewJJJJ"

// let res = order.toLocaleLowerCase()

// console.log(res)

// function flipCoin() {
//   let katpogat = 10;
//   let katgogat = 5;

//   return katpogatkatgogat

//  }

//  let datares = flipCoin();

//  console.warn(datares)


// type Fish = { swim: () => void };
// type Bird = { fly: () => void };
// type Human = {  swim?: () => void, fly?: () => void };

// function move(animal: Fish | Bird | Human) {
//   if ("swim" in animal) { 
// console.log(animal,'if')
      

//   } else {
//    console.log(animal,'else')
      

//   }
// }

// function identity<Type>(arg:Type,name:Type):Type {
//   return arg;
// }

// let output = identity<string>("10","sdf");

// console.log(output,'here is output')



//explicit types

// let character = "sajib";
// let age = 30
// let address = "dhaka"


// //Array 
// let arrays: string[] = []
// arrays.push('sajib')
// console.log(arrays)


// //union 
// let mixed:(string|number|boolean)[] = []
// mixed.push("sajib",34,true)
// console.log(mixed)

// let uid:string|number|boolean

// uid = 34
// uid = "sajib"

// uid = true

// console.log(uid)

// //object
// interface objects {
//   names:string
//   age:number
//   address:string
// }

// let datas:objects

// datas = {
//   names:"sajib",
//   age:10,
//   address:"dhaka"
// }

// console.log(datas.names)

//Dynamic (any) Types

// let age:any ;

// age =25

// age =true

// age = {
// name:'sajib saha'
// }

// console.log(age.name)


// let mixed:any[] = []

// mixed.push('sajib')
// mixed.push('saha')
// mixed.push(34)
// mixed.push(true)

// console.log(mixed)

// let Greet:Function
// let global
// Greet = ()=>{
//   let name = "sajib saha"
//   global = name
// // return global
// }

//  Greet()
// console.log(global) //undefine

// function add(a:number, b:number) {
//     return ab
// }

// let res =  add(10,11)

// console.log(res)


// function optional(a:number,b:number, c?:string|number) {
//   return ab
// }

// let ress = optional(10,5,'ssjfd')

// console.log(ress)



// //type alises

// type stringOrNumber = string|number 
// type ObjectWithName = {name:string , uid:stringOrNumber}

// const logDetails = (uid:stringOrNumber , item:string)=>{
//  console.log(`${12} is active and Item ${item}`)
// }
// logDetails(34,'this');

// const greet = (user:ObjectWithName)=>{
//   console.log(`${user.name} say hello` )
// }

// let obj = {
//  name:'sajib',
//  uid:34
// }

// greet(obj);



//====function signatur=====//

//example one
let greets:(a:string, b:string)=>void;

greets = (name:string, greeting:string)=>{
 // console.log(`${name} ${greeting}`)
 return name
}
let res =  greets('sajib',"say hi")

console.log(res)

//example two
let calculate:(numOne:any, numTwo:number,Action:string)=>number;

calculate = (numone:number, numtwo:number,action:string)=>{
    if(action == 'add'){
     console.log (numone + numtwo)
     return numone - numtwo
    }else{
     console.log (numone - numtwo)
     return numone - numtwo
    }
}

// calculate(10,5,'skdfj') //ans 5
calculate(10,5,'add') //ans 15

//example Three

type stringOrNumber = {name:string,age:string|number} //define types alises

let logDetails :(obj:stringOrNumber)=>void;

logDetails = (tech:stringOrNumber)=>{
  console.log(`${tech.name} and age is ${tech.age}`)
}

let obj = {
 name:"sajib",
 age:24
}

logDetails(obj);