
//==example of using interface == //

interface Isperson {
  name:string
  age:number
  address:string
 speak(a:string):void;
 spend(a:number):number;
}

const sajib : Isperson = {
  name:"sajib",
  age:24,
  address:"dhaka",
  speak(text:string):void {
    console.log(text)
  },
  spend(amount:number):number{
   console.log('i spend' ,amount)
   return amount 
  }

}

console.log(sajib.speak)

//== using same interface in other methods == //

const greeting = (person:Isperson)=>{
 console.log('hello', person.name)
}


greeting(sajib);