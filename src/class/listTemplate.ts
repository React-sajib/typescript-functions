import { Hasformatter } from "./../interfaces/hasFormatter";

export class listTemplate {

 constructor(private scontainer:HTMLUListElement){}


 render(item:Hasformatter , heading:string , pos:'start' | 'end'){

     const li = document.createElement('li')
     const h4 = document.createElement('h4')
      h4.innerText = heading
      li.append(h4)

      const p = document.createElement('p')
      p.innerText = item.format()
      li.append(p)

      if(pos === 'start'){
        this.scontainer.prepend(li)
      }else{
      this.scontainer.append(li)

      }

 }




}