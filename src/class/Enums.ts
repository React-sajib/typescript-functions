//Enum with Generic function

// enum ResourcetYPE  { BOOK,AUTHOR,ADMIN,MEMBER,}

enum ResourcetYPE {
 BOOK = "UP",
 AUTHOR = "DOWN",
 ADMIN = "LEFT",
 MEMBER = "RIGHT",
}


interface resource<T> {
 uid:number
 name:ResourcetYPE
 data:T
}


const resourcevalOne : resource<string> = {
 uid:20,
 name:ResourcetYPE.ADMIN,
 data:"sajib saha"

}
const resourcevalTwo : resource<object> = {
 uid:20,
 name:ResourcetYPE.MEMBER,
 data:{name:'taka',age:undefined}

}
const resourcevalThree : resource<number> = {
 uid:20,
 name:ResourcetYPE.AUTHOR,
 data:56

}

console.log(resourcevalOne.name,
 )


 for (var enumMember in ResourcetYPE) {
  console.log("enum member: ", enumMember); //get the enum poperty name
}


