//genaric function

//==example One==//
// const adduid =  <T extends object>(ob:T)=>{

//  let uid = Math.floor(Math.random() * 100)

//  // let objk = {name:"sajib",age:34}

//  return {...ob, uid}
// }

// let result = adduid({name:"sajib",age:34});
// // let result2 = adduid("sajibsss"); // must be an object

// console.log(result.name)

// //==example Two == //

// const adduidtwo =  <T extends {name:string}>(ob:T)=>{

//  let uid = Math.floor(Math.random() * 100)

//  // let objk = {name:"sajib",age:34}

//  return {...ob, uid}
// }

// let resultr = adduidtwo({name:20,age:34}); //name poperty must be a string


// console.log(resultr.name)


//==generic function with Interface==//

interface Resource<T,P,C,A> {
 uid:T,
 resourceName:P,
 data:C,
 address:A
}


const docThree :Resource<number,string,boolean,object> ={
 uid:45,
 resourceName:"",
 data:true,
 address:{}
}

console.log(docThree)