 import { Hasformatter } from './../interfaces/hasFormatter'


 export class Invoice implements  Hasformatter{
 
    client:string;
    private details:string
    amount:number

    constructor(c:string, b:string , a:number){
        this.client = c
        this.details = b
        this.amount = a
    }

    format (){
        return `${this.client} owns ${this.amount} for ${this.details}`
    }

}